package com.example.job;

import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class FruitProcessor2 implements ItemProcessor<Fruit, Fruit2> {

  @Override
  public Fruit2 process(Fruit item) throws Exception {
    log.info("FruitProcessor2: " + item.toString());
    Fruit2 result = new Fruit2();
    result.setId(new Random().nextInt(10));
    result.setName(item.getName().toUpperCase());
    result.setPrice(item.getPrice());
    return result;
  }
}
