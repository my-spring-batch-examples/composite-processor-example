package com.example.job;

import java.util.Arrays;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.support.CompositeItemProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

@Configuration
@EnableBatchProcessing
public class CompositeProcessorExampleJobConfig {
  private JobBuilderFactory jobBuilderFactory;
  private StepBuilderFactory stepBuilderFactory;

  public CompositeProcessorExampleJobConfig(
      JobBuilderFactory jobBuilderFactory,
      StepBuilderFactory stepBuilderFactory,
      ItemProcessor<Fruit, Fruit> fruitProcessor1) {
    this.jobBuilderFactory = jobBuilderFactory;
    this.stepBuilderFactory = stepBuilderFactory;
  }

  @Bean
  public Job sampleJob(Step sampleStep) {
    return this.jobBuilderFactory.get("compositeProcessorExampleJob").start(sampleStep).build();
  }

  @Bean
  public Step compositeProcessorExampleStep() {
    return stepBuilderFactory
        .get("sampleStep")
        .<Fruit, Fruit2>chunk(10)
        .reader(fruitReader())
        .processor(compositeProcessor())
        .writer(fruitWriter())
        .build();
  }

  @Bean
  @StepScope
  public FlatFileItemReader<Fruit> fruitReader() {
    return new FlatFileItemReaderBuilder<Fruit>()
        .name("fruitReader")
        .resource(new FileSystemResource("input.csv"))
        .delimited()
        .names(new String[] {"name", "price"})
        .fieldSetMapper(
            new BeanWrapperFieldSetMapper<Fruit>() {
              {
                setTargetType(Fruit.class);
              }
            })
        .build();
  }

  @Bean
  public CompositeItemProcessor<Fruit, Fruit2> compositeProcessor() {
    // この書き方だとうまく行かない。delegates.add()にはすべて同じ型を指定する必要がある。
    // そのため、A->B + B-> C のようなCompositeProcessorを設定できない。
    // 参照：https://stackoverflow.com/questions/64009522/spring-batch-how-to-chain-multiple-itemprocessors-with-diffrent-types
    // List<ItemProcessor<Fruit, Fruit2>> delegates = new ArrayList<>(2);
    // delegates.add(new FruitProcessor1());
    // delegates.add(new FruitProcessor2());

    CompositeItemProcessor<Fruit, Fruit2> processor = new CompositeItemProcessor<Fruit, Fruit2>();
    processor.setDelegates(Arrays.asList(new FruitProcessor1(), new FruitProcessor2()));
    return processor;
  }

  @Bean
  @StepScope
  public FlatFileItemWriter<Fruit2> fruitWriter() {
    return new FlatFileItemWriterBuilder<Fruit2>()
        .name("fruitWriter")
        .resource(new FileSystemResource("output.csv"))
        .delimited()
        .names(new String[] {"id", "name", "price"})
        .build();
  }
}
