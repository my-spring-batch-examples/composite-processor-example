package com.example.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompositeProcessorExampleApplication {

  public static void main(String[] args) {
    SpringApplication.run(CompositeProcessorExampleApplication.class, args);
  }
}
